#import <YandexRuntime/YRTI18nManager.h>

@interface YRTI18nManagerFactory : NSObject

/**
 * Returns the locale currently used by the runtime.
 */
+ (void)getLocaleWithLocaleDelegate:(nonnull YRTLocaleDelegate)localeDelegate;


/**
 * Sets the application's locale. Useful only if MapKit is not used by
 * the application. Otherwise, use MapKitFactory.setLocale() or
 * [YMKMapKit setLocale]. Also useless if someone else has already set
 * the locale (produses warning and does nothing). Can be set to none,
 * in this case system locale will be used.
 *
 * Remark:
 * @param locale has optional type, it may be uninitialized.
 */
+ (void)setLocaleWithLocale:(nullable NSString *)locale;


/**
 * Gets the internationalization manager interface.
 */
+ (nonnull YRTI18nManager *)getI18nManagerInstance;


@end
