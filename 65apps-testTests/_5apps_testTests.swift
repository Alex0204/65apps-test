//
//  _5apps_testTests.swift
//  65apps-testTests
//
//  Created by Iskhakov Alexei on 26.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import XCTest
@testable import _5apps_test

class _5apps_testTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
