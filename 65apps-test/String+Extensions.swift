//
//  String+Extensions.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 26.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit

extension String {
    static func isEmpty(_ string: String?) -> Bool {
        guard string != nil else {
            return true
        }
        
        return string!.count == 0
    }
    
    func isValidEmail() -> Bool {
        let emailReges = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
        let predicate = NSPredicate.init(format: "SELF MATCHES %@", emailReges)
        return predicate.evaluate(with: self)
    }
}
