//
//  TextField.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 30.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit
import JVFloatLabeledTextField

class TextField: JVFloatLabeledTextField, UITextFieldDelegate {
    var needsFloatingLabel = false {
        didSet {
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.floatingLabelYPadding = 5
        self.floatingLabelFont = UIFont.systemFont(ofSize: 12)
    }
}
