//
//  HomeViewController.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 29.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit
import YandexMapKit
import CoreLocation

class HomeViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet private weak var ticketButton: UIButton!
    @IBOutlet private weak var mapView: YMKMapView!
    @IBOutlet private weak var currentLocationButton: UIButton!
    @IBOutlet private weak var unlockButton: UIButton!
    @IBOutlet private weak var settigsButton: UIButton!
    @IBOutlet private weak var footerView: UIView!
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ticketButton.layer.cornerRadius = 8
        self.currentLocationButton.layer.cornerRadius = self.currentLocationButton.layer.frame.height/2
        self.unlockButton.layer.cornerRadius = self.unlockButton.layer.frame.height/2
        self.settigsButton.layer.cornerRadius = self.settigsButton.layer.frame.height/2

        self.requestLocation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let maskPath = UIBezierPath.init(roundedRect: self.footerView.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize.init(width: 19.0, height: 19.0))
        let maskLayer = CAShapeLayer.init()
        maskLayer.frame = self.footerView.bounds
        maskLayer.path = maskPath.cgPath
        self.footerView.layer.mask = maskLayer
        self.footerView.layer.masksToBounds = true
        
        let mapMaskPath = UIBezierPath.init(roundedRect: self.mapView.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize.init(width: 19.0, height: 19.0))
        let maskMapLayer = CAShapeLayer.init()
        maskMapLayer.frame = self.mapView.bounds
        maskMapLayer.path = mapMaskPath.cgPath
        self.mapView.layer.mask = maskMapLayer
        self.mapView.layer.masksToBounds = true
    }
    
    //MARK: - Location -
    
    private func requestLocation(){
        self.locationManager.delegate = self
        
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .notDetermined:
            self.locationManager.requestWhenInUseAuthorization()
            self.scrollToDefault()
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            self.locationAuthorizationSucceeded()
        case .restricted: fallthrough
        case .denied:
            self.scrollToDefault()
            print("Геолокация не разрешена")
        default: break
            
        }
    }
    
    private func locationAuthorizationSucceeded() {
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            if let location = self.locationManager.location {
                let latitude = location.coordinate.latitude
                let longitude = location.coordinate.longitude
                self.scrollMap(to: latitude, longitude)
                
                let center = YMKPoint(latitude: latitude, longitude: longitude)
                let mapObjects = self.mapView.mapWindow.map.mapObjects
                mapObjects.addPlacemark(with: center, image: UIImage.init(named: "ic_placemark")!)
            }
        }
    }
    
    private func scrollToDefault(){
        let locValue = YMKPoint(latitude: 54.319763, longitude: 48.404989)
        
        self.mapView.mapWindow.map.move(with: YMKCameraPosition(target: locValue, zoom: 13, azimuth: 0, tilt: 20), animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 0), cameraCallback: nil)
    }
    
    private func scrollMap(to latitude: Double, _ longitude: Double) {
        let locValue = YMKPoint(latitude: latitude, longitude: longitude)
        
        self.mapView.mapWindow.map.move(with: YMKCameraPosition(target: locValue, zoom: 17, azimuth: 0, tilt: 20), animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 0), cameraCallback: nil)
    }
    
    //MARK: - CLLocationManagerDelegate -
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedAlways || status == .authorizedWhenInUse) {
            self.locationAuthorizationSucceeded()
        }
    }
    
    //MARK: -Actions-
    
    @IBAction private func onLocation() {
        self.locationAuthorizationSucceeded()
    }
}
