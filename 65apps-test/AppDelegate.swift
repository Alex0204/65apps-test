//
//  AppDelegate.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 26.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import UIKit
import YandexMapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        YMKMapKit.setApiKey("e72ce954-f064-4fdc-8367-f6d98962cc6d")
        self.showRoot()
        return true
    }
    
    func showRoot(){
        if let window = self.window {
            let storyboard = UIStoryboard.init(name: "Authorization", bundle: nil)
            window.rootViewController = storyboard.instantiateInitialViewController()
        }
    }


}

