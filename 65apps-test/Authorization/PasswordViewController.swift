//
//  PasswordViewController.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 30.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit

class PasswordViewController: UIViewController {
    @IBOutlet private weak var passwordTextField: TextField!
    @IBOutlet private weak var nextButton: UIButton!
    
    var email: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.passwordTextField.needsFloatingLabel = true
        self.passwordTextField.floatingLabelYPadding = 8
        self.passwordTextField.setNeedsLayout()
        self.passwordTextField.layoutIfNeeded()
        self.passwordTextField.floatingLabelTextColor = UIColor.mainAccent
        
        self.nextButton.layer.borderWidth = 1
        self.nextButton.layer.borderColor = UIColor.mainAccent.cgColor
        self.nextButton.layer.cornerRadius = self.nextButton.frame.size.height/2
    }
    
    private func validate() -> Bool {
        let alertController = UIAlertController(title: "Внимание", message: "Пароль должен содержать не менее 6 символов", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in }
        alertController.addAction(action)
        
        guard self.passwordTextField.text?.count ?? 0 >= 6 else {
            self.present(alertController, animated: true, completion: nil)
            return false 
        }
        
        return true
    }
    
    @IBAction private func secure(){
        self.passwordTextField.isSecureTextEntry = !self.passwordTextField.isSecureTextEntry
    }
    
    @IBAction func onHome() {
        if self.validate() {
            self.performSegue(withIdentifier: "PasswordToHomeSegue", sender: self)
        }
    }
    
}
