//
//  NameViewController.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 26.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit

class NameViewController: UIViewController {
    @IBOutlet private weak var firstNameTextField: TextField!
    @IBOutlet private weak var lastNameTextField: TextField!
    @IBOutlet private weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstNameTextField.needsFloatingLabel = true
        self.firstNameTextField.floatingLabelYPadding = 8
        self.firstNameTextField.setNeedsLayout()
        self.firstNameTextField.layoutIfNeeded()
        self.firstNameTextField.floatingLabelTextColor = UIColor.mainAccent
        
        self.lastNameTextField.needsFloatingLabel = true
        self.lastNameTextField.floatingLabelYPadding = 8
        self.lastNameTextField.setNeedsLayout()
        self.lastNameTextField.layoutIfNeeded()
        self.lastNameTextField.floatingLabelTextColor = UIColor.mainAccent
        
        self.nextButton.layer.borderWidth = 1
        self.nextButton.layer.borderColor = UIColor.mainAccent.cgColor
        self.nextButton.layer.cornerRadius = self.nextButton.frame.size.height/2
    }
    
    private func validate() -> Bool {
        let firstNameAlertController = UIAlertController(title: "Внимание", message: "Поле FirstName пустое", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in }
        firstNameAlertController.addAction(action)
        
        let lastNameAlertController = UIAlertController(title: "Внимание", message: "Поле LastName пустое", preferredStyle: .alert)
        
        lastNameAlertController.addAction(action)
        
        guard !String.isEmpty(self.firstNameTextField.text ?? "") else {
            self.present(firstNameAlertController, animated: true, completion: nil)
            return false
        }
        
        guard !String.isEmpty(self.lastNameTextField.text ?? "") else {
            self.present(lastNameAlertController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func onEmail() {
        if self.validate() {
            self.performSegue(withIdentifier: "NameToEmailSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NameToEmailSegue" {
            if let emailVC = segue.destination as? EmailViewController {
                emailVC.firstName = self.firstNameTextField.text
                emailVC.lastName = self.lastNameTextField.text
            }
        }
    }
}
