//
//  EmailViewController.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 26.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManager

class EmailViewController: UIViewController {
    @IBOutlet private weak var emailTextField: TextField!
    @IBOutlet private weak var nextButton: UIButton!
    
    var firstName: String?
    var lastName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.needsFloatingLabel = true
        self.emailTextField.floatingLabelYPadding = 8
        self.emailTextField.setNeedsLayout()
        self.emailTextField.layoutIfNeeded()
        self.emailTextField.floatingLabelTextColor = UIColor.mainAccent
        
        self.nextButton.layer.borderWidth = 1
        self.nextButton.layer.borderColor = UIColor.mainAccent.cgColor
        self.nextButton.layer.cornerRadius = self.nextButton.frame.size.height/2
       
    }
    
    private func validate() -> Bool {
        let alertController = UIAlertController(title: "Внимание", message: "Поле заполнено не корректно", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in }
        alertController.addAction(action)
        
        guard (self.emailTextField.text?.isValidEmail())! else {
            self.present(alertController, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    @IBAction func onPassword() {
        if self.validate() {
            self.performSegue(withIdentifier: "EmailToPasswordSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmailToPasswordSegue" {
            if let passVC = segue.destination as? PasswordViewController {
                passVC.email = self.emailTextField.text
            }
        }
    }
}
