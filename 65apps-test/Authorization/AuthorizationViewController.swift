//
//  AuthorizationViewController.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 26.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit

class AuthorizationViewController: UIViewController {
    @IBOutlet private weak var createButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createButton.layer.borderWidth = 1
        self.createButton.layer.borderColor = UIColor.mainAccent.cgColor
        self.createButton.layer.cornerRadius = self.createButton.frame.size.height/2
        
    }
    
    @IBAction func onCreate() {
        self.performSegue(withIdentifier: "WelcomeToCreateSegue", sender: self)
    }

}
