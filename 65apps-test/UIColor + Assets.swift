//
//  UIColor + Assets.swift
//  65apps-test
//
//  Created by Iskhakov Alexei on 26.08.2020.
//  Copyright © 2020 Iskhakov Alexei. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    public class var mainAccent: UIColor {
        return UIColor(named: "main_accent")!
    }
}
